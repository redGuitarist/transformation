#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <GLFW/glfw3.h>


// Color - stores color as 3 doubles from 0 to 1
struct Color
{
    double r, g, b;
};


// Vector2 - a simple functional 2D vector
//  contains 2 coordinates - x and y
//  TODO: make a template(?)
class Vector2
{
public:
    double x, y;

    Vector2(double x = 0, double y = 0) { this->x = x; this->y = y; }

    // add and subtract vectors
    Vector2 operator+(const Vector2& v) { return Vector2(this->x + v.x, this->y + v.y); }
    Vector2 operator-(const Vector2& v) { return Vector2(this->x - v.x, this->y - v.y); }
        
	// multiply and divide vector by number
    Vector2 operator*(double c) { return Vector2(x * c, y * c); }
	friend Vector2 operator*(double c, Vector2 vec) { return vec * c; }
    Vector2 operator/(double c) { return Vector2(x / c, y / c); }
	friend Vector2 operator/(double c, Vector2 vec) { return vec / c; }
};


// Matrix3 - a 3x3 matrix
//  used for transformations
class Matrix3
{
public:

    double matrix[3][3];

    // construct a matrix
    Matrix3(double arr[3][3])
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                matrix[i][j] = arr[i][j];
    }

    Matrix3(const Matrix3& m3)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                matrix[i][j] = m3.matrix[i][j];
    }

    Matrix3(double m00 = 0, double m01 = 0, double m02 = 0,
        double m10 = 0, double m11 = 0, double m12 = 0,
        double m20 = 0, double m21 = 0, double m22 = 0)
    {
        matrix[0][0] = m00; matrix[0][1] = m01; matrix[0][2] = m02;
        matrix[1][0] = m10; matrix[1][1] = m11; matrix[1][2] = m12;
        matrix[2][0] = m20; matrix[2][1] = m21; matrix[2][2] = m22;
    }

    // multiply Matrix3 by Vector2
    Vector2 operator*(const Vector2& vec) const
    {
        Vector2 result;
        result.x = matrix[0][0] * vec.x + matrix[0][1] * vec.y + matrix[0][2] * 1; // 1 is
        result.y = matrix[1][0] * vec.x + matrix[1][1] * vec.y + matrix[1][2] * 1; // vec.w
        return result;
    }

    // multiply Matrix3 by Matrix3 - superposition
    Matrix3 operator*(const Matrix3& matrix) const
    {
        Matrix3 newmatrix;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    newmatrix.matrix[i][j] += this->matrix[i][k] * matrix.matrix[k][j];

        return newmatrix;
    }

    // Matrix3::translation - get a Matrix3 performing
    //  translation by xTrans and yTrans
    static Matrix3 translation(double xTrans, double yTrans)
    {
        return Matrix3(1, 0, xTrans,
            0, 1, yTrans,
            0, 0, 1);
    }
    // Matrix3::rotation - get a Matrix3 performing
    //  rotation by angle (in radians)
    static Matrix3 rotation(double angle)
    {
        return Matrix3(std::cos(angle), -std::sin(angle), 0,
            std::sin(angle), std::cos(angle), 0,
            0, 0, 1);
    }
    // Matrix3::scaling - get a Matrix3 performing
    //  scale by xFactor and yFactor
    static Matrix3 scaling(double xFactor, double yFactor)
    {
        return Matrix3(xFactor, 0, 0,
            0, yFactor, 0,
            0, 0, 1);
    }

    static Matrix3 I;
};
Matrix3 Matrix3::I(1, 0, 0,
    0, 1, 0,
    0, 0, 1);


// Shape - represents a polygon
class Shape
{
private:
    std::vector<Vector2> points;

public:

    // construct from initializer_list
    Shape(const std::initializer_list<Vector2>& list)
    {
        for (auto point : list)
            points.push_back(point);
    }

    // construct from vector
    Shape(const std::vector<Vector2>& list)
    {
        for (auto point : list)
            points.push_back(point);
    }

    // return size
    int size() const { return points.size(); }

    // return point at i performing boundaries checking
    Vector2 at(int i) const
    {
        return points.at(i);
    }

    // apply transformation
    void transform(Matrix3 matrix)
    {
        for (size_t i = 0; i < points.size(); i++)
            points[i] = matrix * points[i];
    }

    // get vector of points
    std::vector<Vector2>& getVec()
    {
        return points;
    }
};


// Drawer - draws graphical primitives
//  automatically handling all transformations
class Drawer
{
private:
    Matrix3 matrix;
    Matrix3 savedmatrix;

public:
    Drawer() :matrix(Matrix3::I), savedmatrix(Matrix3::I) {}

    // save transformation
    void save() { savedmatrix = matrix; }

    // restore to saved state
    void restore() { matrix = savedmatrix; }

    // restore to initial state
    void reset() { matrix = Matrix3::I; }

    // return current transformation matrix
    Matrix3 getMatrix() { return matrix; }

    // make all next drawings rotated
    void rotate(double alpha, bool toEnd = false)
    {
        if (toEnd)
            matrix = matrix * Matrix3::rotation(alpha);
        else
            matrix = Matrix3::rotation(alpha) * matrix;
    }

    // make all next drawings translated
    void translate(double x, double y, bool toEnd = false)
    {
        if (toEnd)
            matrix = matrix * Matrix3::translation(x, y);
        else
            matrix = Matrix3::translation(x, y) * matrix;
    }

    // make all next drawings scaled
    void scale(double x, double y, bool toEnd = false)
    {
        if (toEnd)
            matrix = matrix * Matrix3::scaling(x, y);
        else
            matrix = Matrix3::scaling(x, y) * matrix;
    }

    // draw a line from bg to end
    void draw(Vector2 bg, Vector2 end) const
    {
        // do the transformation
        bg = matrix * bg;
        end = matrix * end;

        glBegin(GL_LINES);

        glVertex3d(bg.x, bg.y, 0);
        glVertex3d(end.x, end.y, 0);

        glEnd();
    }

    // draw a point at pt
    void draw(Vector2 pt) const
    {
        // do the transformation
        pt = matrix * pt;

        glBegin(GL_POINTS);

        glVertex3d(pt.x, pt.y, 0);

        glEnd();
    }

    // draw a filled shape
    void draw(const Shape& sh) const
    {
        glBegin(GL_POLYGON);

        for (int i = 0; i < sh.size(); i++)
        {
            Vector2 newvec = matrix * sh.at(i);
            glVertex3d(newvec.x, newvec.y, 0);
        }

        Vector2 newvec = matrix * sh.at(0);
        glVertex3d(newvec.x, newvec.y, 0);

        glEnd();
    }
};
