#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstdio>
#include <cmath>
#include <GLFW/glfw3.h>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "transformation.hpp"

using namespace std;
double PI = acos(-1);

vector<pair<Shape, Color>> shapes;
bool isInputtingShape = false;
Color currColor;
vector<Vector2> currShape;

double transX, transY, scaleX, scaleY, rot;
int width, height;

// create drawer
Drawer drawer;

bool shouldClose = false;

void printHelp();
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void draw();
void write(string file);
void processCommands(istream& is, bool prompt = true);

int main(int argc, char** argv)
{
    // if we have a cli option, process it first
    if (argc > 1)
    {
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
        {
            cout << "Transformation: performs 2D graphical transformation\n"
             << "CLI usage:\n"
             << "\ttransformation -h|--help  - print this and exit\n"
             << "\ttransformation [file]          - process batch file (open interactive, if not present)\n";
            exit(0);
        }
        ifstream ifile(argv[1]);
        if (ifile.is_open())
            processCommands(ifile, false);
        else
            cerr << "File not found" << endl;

        ifile.close();
    }

    //printHelp();
    cout << "Transformation: performs 2D graphical transformation\n"
             << "\nInteractive shell\n\n"
             << "enter 'h' for a list of avialable commands\n";

    // start processing
    processCommands(cin);
}


// callback function for handling keypress in graphics window
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS && action != GLFW_REPEAT)
        return;

    if (key == GLFW_KEY_KP_ADD)
    {
        //scaleX *= 1.1;
        //scaleY *= 1.1;
        drawer.scale(1.1, 1.1);
    }
    else if (key == GLFW_KEY_KP_SUBTRACT)
    {
        //scaleX /= 1.1;
        //scaleY /= 1.1;
        drawer.scale(1/1.1, 1/1.1);
    }
    else if (key == GLFW_KEY_UP)
        //transY += 5;
        drawer.translate(0, 5);
    else if (key == GLFW_KEY_DOWN)
        //transY -= 5;
        drawer.translate(0, -5);
    else if (key == GLFW_KEY_LEFT)
        //transX -= 5;
        drawer.translate(-5, 0);
    else if (key == GLFW_KEY_RIGHT)
        //transX += 5;
        drawer.translate(5, 0);
    else if (key == GLFW_KEY_KP_3)
        //rot -= 5;
        drawer.rotate(-5 * PI / 180);
    else if (key == GLFW_KEY_KP_1)
        //rot += 5;
        drawer.rotate(5 * PI / 180);
    else if (key == GLFW_KEY_KP_7)
        //scaleX *= 1.1;
        drawer.scale(1.1, 1);
    else if (key == GLFW_KEY_KP_4)
        //scaleX /= 1.1;
        drawer.scale(1/1.1, 1);
    else if (key == GLFW_KEY_KP_9)
        //scaleY *= 1.1;
        drawer.scale(1, 1.1);
    else if (key == GLFW_KEY_KP_6)
        //scaleY /= 1.1;
        drawer.scale(1, 1/1.1);
    else if (key == GLFW_KEY_KP_5)
    {
        /*scaleX = 1;
        scaleY = 1;
        rot = 0;
        transX = width / 2.0;
        transY = height / 2.0;*/
        drawer.reset();
    }
    else if (key == GLFW_KEY_ESCAPE)
        glfwSetWindowShouldClose(window, GL_TRUE);
}


// function to print help
void printHelp()
{
    cout << "\nHelp              - print this\n"
        << "Begin      r g b  - begin new shape with color (r, g, b) in range [0, 1]\n"
        << "Vertex     x y    - add a vertex to current shape\n"
        << "End               - end current shape\n"
        << "Copy       r g b  - copy last shape to current with new color\n"
        << "Delete            - delete last shape\n"
        << "transLate  x y    - translate last shape\n"
        << "Scale      x y    - scale last shape\n"
        << "roTater    phi    - rotate last shape (phi in radians)\n"
        << "Rotate     phi    - rotate last shape (phi in degrees)\n"
        << "draW              - draw all shapes in new window, where:\n"
        << "                    '+' and '-'   - control proportional scaling\n"
        << "                                    (default is 1:1px)\n"
        << "                    arrows        - move shapes\n"
        << "                    NUM1 and NUM3 - rotate (counter)clockwise\n"
        << "                    NUM7 and NUM4 - +- x scaling\n"
        << "                    NUM9 and NUM6 - +- y scaling\n"
        << "                    NUM5          - reset\n"
        << "                    ESC           - close window\n"
        << "wrIte      file   - write all shapes to file\n"
        << "reAd       file   - process commands from file\n"
        << "eXit              - end the program\n"
        << "\n"
        << "one-letter shortcut can be used instead of full command (specified as capital)\n"
        << endl;
}


// function that opens a window and draws shapes
void draw()
{
    if (!glfwInit())
    {
        cerr << "Error loading GLFW\n";
        return;
    }

    // create a window
    GLFWwindow* window = glfwCreateWindow(640, 480, "Transform", NULL, NULL);
    if (!window)
    {
        cerr << "Unable to create window!" << endl;
        return;
    }
    
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);

    // set current context
    glfwMakeContextCurrent(window);

    // configure viewport
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, width, 0.0, height, 0.0, 1.0);

    // register callback
    glfwSetKeyCallback(window, keyCallback);

    transX = width / 2.0;
    transY = height / 2.0;
    scaleX = 1;
    scaleY = 1;
    rot = 0;

    // set up scaling and translating

    while (!glfwWindowShouldClose(window))
    {
        // clear screen
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // draw here
        for (auto shapeColor : shapes)
        {
            drawer.save();
            drawer.scale(scaleX, scaleY);
            drawer.rotate(rot * PI / 180);
            drawer.translate(transX, transY);
            // set color
            glColor3d(shapeColor.second.r, shapeColor.second.g, shapeColor.second.b);

            // draw a shape
            drawer.draw(shapeColor.first);
            drawer.restore();
        }

        // proceed to next frame
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    
    // free resources
    glfwTerminate();
}


// function that outputs all shapes to file
void write(string file)
{
    ofstream os(file);
    if (!os.is_open())
        cerr << "File cannot be opened!\n";
    else
    {
        for (auto shapeColor : shapes)
        {
            os << "b " << shapeColor.second.r << ' ' << shapeColor.second.g << ' ' << shapeColor.second.b << endl;
            for (int i = 0; i < shapeColor.first.size(); i++)
                os << "v " << shapeColor.first.at(i).x << ' ' << shapeColor.first.at(i).y << endl;
            os << "e" << endl;
        }
    }
}


// function that processes commands from CLI
void processCommands(istream& is, bool prompt)
{
    while (!shouldClose && !is.eof())
    {
        if (prompt)
            cout << ">> ";

        // read and parse commands
        string cmd, params;
        getline(is, params);
        cmd = params.substr(0, params.find(" "));
        params = params.erase(0, params.find(" "));

        // make string lowercase
        transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower);
        // clear whitespaces at start
        params.erase(params.begin(), find_if(params.begin(), params.end(), [](int ch) {
            return !isspace(ch);
        }));

        if (cmd == "" || cmd[0] == '#')
            continue;
        else if (cmd == "help" || cmd == "h")
            printHelp();
        else if (cmd == "exit" || cmd == "x")
        {
            shouldClose = true;
            break;
        }
        else if (cmd == "begin" || cmd == "b")
        {
            if (isInputtingShape)
                cerr << "Invalid command: you must first end a previous shape\n";
            else
            {
                double r, g, b;
                // validate input
                if (sscanf(params.c_str(), "%lf %lf %lf", &r, &g, &b) < 3 || r > 1 || g > 1 || b > 1
                    || r < 0 || g < 0 || b < 0)
                    cerr << "Invalid command: wrong params\n";
                else
                {
                    isInputtingShape = true;
                    currColor = { r, g, b };
                    currShape.clear();
                }
            }
        }
        else if (cmd == "vertex" || cmd == "v")
        {
            if (!isInputtingShape)
                cerr << "Incorrect command: you must first begin a shape\n";
            else
            {
                double x, y;
                // validate input
                if (sscanf(params.c_str(), "%lf %lf", &x, &y) < 2)
                    cerr << "Invalid command: wrong params\n";
                else
                    currShape.emplace_back(x, y);
            }
        }
        else if (cmd == "end" || cmd == "e")
        {
            if (!isInputtingShape)
                cerr << "Incorrect command: you must first begin a shape\n";
            else
            {
                shapes.emplace_back(vector<Vector2>(currShape), currColor);
                isInputtingShape = false;
            }
        }
        else if (cmd == "draw" || cmd == "w")
        {
            if (isInputtingShape)
                cerr << "Warning! You have a not ended shape, it won't be displayed\n";
            draw();
        }
        else if (cmd == "copy" || cmd == "c")
        {
            if (isInputtingShape)
                cerr << "Invalid command: you must first end a previous shape\n";
            else
            {
                double r, g, b;
                // validate input
                if (sscanf(params.c_str(), "%lf %lf %lf", &r, &g, &b) < 3 || r > 1 || g > 1 || b > 1
                    || r < 0 || g < 0 || b < 0)
                    cerr << "Invalid command: wrong params\n";
                else
                {
                    isInputtingShape = true;
                    if (!shapes.empty())
                        currShape = shapes.back().first.getVec();
                    currColor = { r, g, b };
                }
            }
        }
        else if (cmd == "delete" || cmd == "d")
        {
            if (!shapes.empty())
                shapes.pop_back();
        }
        else if (cmd == "rotater" || cmd == "t")
        {
            if (isInputtingShape)
                cerr << "Incorrect command: you must first end a previous shape\n";
            else
            {
                double phi;
                // validate input
                if (sscanf(params.c_str(), "%lf", &phi) < 1)
                    cerr << "Invalid command: wrong params\n";
                else
                    shapes.back().first.transform(Matrix3::rotation(phi));
            }
        }
        else if (cmd == "rotate" || cmd == "r")
        {
            if (isInputtingShape)
                cerr << "Incorrect command: you must first end a previous shape\n";
            else
            {
                double phi;
                // validate input
                if (sscanf(params.c_str(), "%lf", &phi) < 1)
                    cerr << "Invalid command: wrong params\n";
                else
                    shapes.back().first.transform(Matrix3::rotation(phi * PI / 180));
            }
        }
        else if (cmd == "translate" || cmd == "l")
        {
            if (isInputtingShape)
                cerr << "Incorrect command: you must first end a previous shape\n";
            else
            {
                double x, y;
                // validate input
                if (sscanf(params.c_str(), "%lf %lf", &x, &y) < 2)
                    cerr << "Invalid command: wrong params\n";
                else
                    shapes.back().first.transform(Matrix3::translation(x, y));
            }
        }
        else if (cmd == "scale" || cmd == "s")
        {
            if (isInputtingShape)
                cerr << "Incorrect command: you must first end a previous shape\n";
            else
            {
                double x, y;
                // validate input
                if (sscanf(params.c_str(), "%lf %lf", &x, &y) < 2)
                    cerr << "Invalid command: wrong params\n";
                else
                    shapes.back().first.transform(Matrix3::scaling(x, y));
            }
        }
        else if (cmd == "write" || cmd == "i")
        {
            if (isInputtingShape)
                cerr << "Warning! You have a not ended shape, it won't be written\n";
            write(params);
        }
        else if (cmd == ":3")
            cout << "I love my tester :3\n";
        else if (cmd == "read" || cmd == "a")
        {
            ifstream ifile(params);
            if (!ifile.is_open())
                cerr << "File cannot be opened!\n";
            else
            {
                processCommands(ifile, false);
                cout << "\nFile has been read" << endl;
            }
        }
        else
            cerr << "Unknown command\n";
    }
}
