CXXFILES = main.cpp
CXXFLAGS = -o transformation -std=c++17 -Wall -Wextra
ifeq ($(OS),Windows_NT)
	CXXLIBS += -L./lib-mingw libglfw3.q
	CXXINCLUDES += -I./include
else
	CXXLIBS += -lGL -lglfw
endif

all: 
	${CXX} ${CXXFLAGS} ${CXXFILES} ${CXXLIBS} ${CXXINCLUDES}
clean:
	rm -f *.o transformation
